package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.ModelMap;
import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@RestController
public class DemoApplication {

	String style = "<style type='text/css'>" + "</style>";

	@GetMapping("/")
	String home() {
        // TODO - Personalize this message!
        System.out.println("handling GET");        
        String body =
                "<body>" +
                "</body>";

        return style + body;
	}

	@GetMapping("/validate")
	String validateFtn() {
        // TODO - Personalize this message!
        System.out.println("handling GET validate");        
        String body =
                "<body>" +
                "</body>";

        return style + body;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
